/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Benchmark implementations of available diffraction models.
 *
 * ----------------------------------------------------------------
 */

#include <ITAException.h>
#include <ITAFiniteImpulseResponse.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/ModelBase.h>
#include <ITAGeo/Scene.h>
#include <ITAGeo/Utils.h>
#include <ITAHDFTSpectrum.h>
#include <ITAPropagationModels/Base.h>
#include <ITAPropagationModels/Maekawa.h>
#include <ITAPropagationModels/Svensson.h>
#include <ITAPropagationModels/UTD.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <VistaBase/VistaVector3D.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <iostream>

using namespace std;
using namespace ITAPropagationModels;

float g_fSpeedOfSound = ITAConstants::DEFAULT_SPEED_OF_SOUND_F;
float g_fSampleRate   = 44.1e3;
int g_iFilterLength   = 64;

void benchmark_single_wedge( );

int main( int, char* )
{
	try
	{
		benchmark_single_wedge( );
	}
	catch( ITAException& e )
	{
		cerr << e << endl;
		return 255;
	}

	return 0;
}

void benchmark_single_wedge( )
{
	cout << "Benchmarking diffraction models" << endl;

	auto oSource     = std::make_shared<ITAGeo::CEmitter>( );
	oSource->sName   = "Benchmark 1 source";
	oSource->qOrient = VistaQuaternion( 0, -1, 0, 0 );
	oSource->vPos.SetValues( 1.1f, .9f, .9f );

	auto oReceiver     = std::make_shared<ITAGeo::CSensor>( );
	oReceiver->sName   = "Benchmark 1 receiver";
	oReceiver->qOrient = VistaQuaternion( 0, 1, 0, 0 );
	oReceiver->vPos.SetValues( -.4f, .6f, .1f );

	auto pWedge                       = std::make_shared<ITAGeo::CITADiffractionOuterWedgeAperture>( );
	pWedge->v3MainWedgeFaceNormal     = VistaVector3D( 1.0f, 1.0f, .0f ).GetNormalized( );
	pWedge->v3OppositeWedgeFaceNormal = VistaVector3D( -1.0f, 1.0f, .0f ).GetNormalized( );
	pWedge->v3VertextStart.SetValues( .0f, 1.0f, -1.0f );
	pWedge->v3VertextEnd.SetValues( .0f, 1.0f, 1.0f );

	ITAGeoUtils::CalculateDiffractionAperturePoint( pWedge->GetEdgeRay( ), oSource->v3InteractionPoint, oReceiver->v3InteractionPoint, pWedge->v3AperturePoint );

	if( !ITAGeoUtils::IsDiffractionAperturePointInRange( pWedge->v3VertextStart, pWedge->v3VertextEnd, pWedge->v3AperturePoint ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Aperture point over wedge is outside edge vertices" );

	if( !pWedge->IsOutsideWedge( oSource->v3InteractionPoint ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Source point is inside wedge (solid part)" );

	if( !pWedge->IsOutsideWedge( oReceiver->v3InteractionPoint ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Receiver point is inside wedge (solid part)" );

	ITAGeo::CPropagationPath oPropPath;
	oPropPath.push_back( oSource );
	oPropPath.push_back( pWedge );
	oPropPath.push_back( oReceiver );

	cout << "Path: " << oPropPath << endl;

	// Kirchhoff

	if( Maekawa::IsApplicable( oSource->v3InteractionPoint, oReceiver->v3InteractionPoint, pWedge ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Kirchhoff model can not be applied for this propagation path." );

	ITAStopWatch sw;

	cout << "Starting Kirchoff model benchmark:" << endl;
	double dStartTime = ITAClock::getDefaultClock( )->getTime( );

	ITABase::CHDFTSpectrum oTF( g_fSampleRate, g_iFilterLength, true );
	for( int i = 0; i < 1e8; i++ )
	{
		sw.start( );
		double dDir, dDet;
		Maekawa::GetDirectLengthAndDetourLength( oSource->v3InteractionPoint, oReceiver->v3InteractionPoint, pWedge, dDir, dDet );
		Maekawa::CalculateDiffractionFilter( dDir, dDet, oTF, g_fSpeedOfSound );
		sw.stop( );
	}
	double dStopTime       = ITAClock::getDefaultClock( )->getTime( );
	double dProcessingTime = dStopTime - dStartTime;
	cout << "\tBenchmark took " << timeToString( dProcessingTime ) << endl;
	cout << "\tStatistics:" << sw << endl;
	cout << "\tResult:" << oTF.ToString( ) << endl;
	cout << endl;

	sw.reset( );
	cout << "Starting UTD model benchmark:" << endl;
	dStartTime        = ITAClock::getDefaultClock( )->getTime( );
	const int iMethod = UTD::UTD_APPROX_KAWAI_KOUYOUMJIAN;
	for( int i = 0; i < 1e6; i++ )
	{
		sw.start( );
		UTD::CalculateDiffractionFilter( oSource->v3InteractionPoint, oReceiver->v3InteractionPoint, pWedge, oTF, iMethod, g_fSpeedOfSound );
		sw.stop( );
	}
	dStopTime       = ITAClock::getDefaultClock( )->getTime( );
	dProcessingTime = dStopTime - dStartTime;
	cout << "\tBenchmark took " << timeToString( dProcessingTime ) << endl;
	cout << "\tStatistics:" << sw << endl;
	cout << "\tResult:" << oTF.ToString( ) << endl;
	cout << endl;

	sw.reset( );
	cout << "Starting Biot-Tolstoy-Medwin-Svensson model benchmark:" << endl;
	dStartTime            = ITAClock::getDefaultClock( )->getTime( );
	const float fMinDelay = pWedge->GetMinimumWavefrontDelayTime( oSource->v3InteractionPoint, oReceiver->v3InteractionPoint, g_fSpeedOfSound );
	const float fMaxDelay = pWedge->GetMaximumWavefrontDelayTime( oSource->v3InteractionPoint, oReceiver->v3InteractionPoint, g_fSpeedOfSound );
	const int iIRLength   = (int)ceil( float( fMaxDelay - fMinDelay ) * g_fSampleRate );
	ITABase::CFiniteImpulseResponse oIR( iIRLength, g_fSampleRate, true );
	for( int i = 0; i < 1e4; i++ )
	{
		sw.start( );
		Svensson::CalculateDiffractionIR( oSource->v3InteractionPoint, oReceiver->v3InteractionPoint, pWedge, oIR, g_fSpeedOfSound );
		sw.stop( );
	}
	dStopTime       = ITAClock::getDefaultClock( )->getTime( );
	dProcessingTime = dStopTime - dStartTime;
	cout << "\tBenchmark took " << timeToString( dProcessingTime ) << endl;
	cout << "\tStatistics:" << sw << endl;
	cout << "\tResult:" << oIR.toString( ) << endl;
	cout << endl;
}
