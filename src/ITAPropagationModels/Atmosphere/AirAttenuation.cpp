#include <ITAException.h>
#include <ITAISO9613.h>
#include <ITAPropagationModels/Atmosphere/AirAttenuation.h>
#include <algorithm>
#include <cmath>


using namespace ITAPropagationModels;

ITABase::CThirdOctaveFactorMagnitudeSpectrum Atmosphere::AirAttenuationSpectrum( const ITAPropagationPathSim::AtmosphericRayTracing::CRay& oRay,
                                                                                 const ITAGeo::CStratifiedAtmosphere& oAtmosphere, const int iDownSamplingFactor /*= 1*/ )
{
	ITABase::CThirdOctaveFactorMagnitudeSpectrum outputSpectrum;
	AirAttenuationSpectrum( outputSpectrum, oRay, oAtmosphere, iDownSamplingFactor );

	return outputSpectrum;
}

void Atmosphere::AirAttenuationSpectrum( ITABase::CThirdOctaveFactorMagnitudeSpectrum& outputSpectrum, const ITAPropagationPathSim::AtmosphericRayTracing::CRay& oRay,
                                         const ITAGeo::CStratifiedAtmosphere& oAtmosphere, const int iDownSamplingFactor /*= 1*/ )
{
	if( oRay.NumPoints( ) < 2 )
		ITA_EXCEPT_INVALID_PARAMETER( "Ray must at least have two points." );


	ITABase::CThirdOctaveDecibelMagnitudeSpectrum oTotalDecibelSpectrum;
	oTotalDecibelSpectrum.SetIdentity( );
	for( int idxCurrent = 0; idxCurrent < oRay.NumPoints( ) - 1; idxCurrent += iDownSamplingFactor )
	{
		const int idxNext = std::min( idxCurrent + iDownSamplingFactor, oRay.NumPoints( ) - 1 );

		const double dAltitude = (double)std::abs( oRay[idxCurrent].position[Vista::Z] );
		const double dDistance = (double)( oRay[idxNext].position - oRay[idxCurrent].position ).GetLength( );

		const double dTemperaturCelsius = oAtmosphere.Temperature( dAltitude ) - 273.15;  //�C
		const double dRelativeHumidity  = oAtmosphere.RelativeHumidity( dAltitude );      // %
		const double dStaticPressure    = oAtmosphere.StaticPressure( dAltitude ) / 1000; // kPA

		ITABase::CThirdOctaveDecibelMagnitudeSpectrum oTmpSpectrum;
		ITABase::ISO9613::AtmosphericAbsorption( oTmpSpectrum, dDistance, dTemperaturCelsius, dRelativeHumidity, dStaticPressure );

		oTotalDecibelSpectrum.Add( oTmpSpectrum );
	}


	for( int idx = 0; idx < oTotalDecibelSpectrum.GetNumBands( ); idx++ )
		outputSpectrum[idx] = pow( 10, -oTotalDecibelSpectrum[idx] / 20.0 );
}
