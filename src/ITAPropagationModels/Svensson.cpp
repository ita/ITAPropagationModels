#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAGeo/Utils.h>
#include <ITAPropagationModels/Svensson.h>
#include <cassert>

using namespace ITAConstants;
using namespace ITAPropagationModels;

//! Heaviside's unit step function
float SvenssonHelperHeavisideFunction( const float fX );

//! Helper function to evaluate Svensson's integral solution at given time
float SvenssonHelperIntegralSolution( const float t, const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos,
                                      std::shared_ptr<const ITAGeo::CITADiffractionWedgeApertureBase> pApex, const float fSpeedOfSound );

//! Midpoint approximation to evalute Svensson's integral
float SvenssonHelperIntegralMidpointApproximation( const float t );

//! Helper function defining the beta variable in svensson integral formulation
float SvenssonHelperBeta( const float nu, const float theta_s, const float theta_r, const float alpha, const float gamma );

//! Plus-minus variant for beta values in geometrical functions
float SvenssonHelperBetaEval( const float nu, const float alpha, const float gamma, const float fPMVariant );

bool Svensson::CalculateDiffractionIR( const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos,
                                       std::shared_ptr<const ITAGeo::CITADiffractionWedgeApertureBase> pApex, ITABase::CFiniteImpulseResponse& oEffectiveDiffractionIR,
                                       const float fSpeedOfSound /* = ITAConstants::SPEED_OF_SOUND_F */ )
{
	if( fSpeedOfSound <= 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Speed of sound must be greate zero" );

	if( ITAGeoUtils::IsPointInsideDiffrationWedge( pApex->v3VertextStart, pApex->v3VertextEnd, pApex->v3MainWedgeFaceNormal, pApex->v3OppositeWedgeFaceNormal,
	                                               v3SourcePos ) )
		return false;

	if( ITAGeoUtils::IsPointInsideDiffrationWedge( pApex->v3VertextStart, pApex->v3VertextEnd, pApex->v3MainWedgeFaceNormal, pApex->v3OppositeWedgeFaceNormal,
	                                               v3TargetPos ) )
		return false;

	// Iterate over effective IR samples (latency compensated) and solve Svensson integral or approximation
	assert( oEffectiveDiffractionIR.GetSampleRate( ) > 0.0f );
	const float fIntegrationSetpSize = float( 1.0f / oEffectiveDiffractionIR.GetSampleRate( ) ); // also referred to as \Tau
	for( int i = 0; i < oEffectiveDiffractionIR.GetLength( ); i++ )
	{
		const float t              = i * fIntegrationSetpSize; // Iteration step time
		oEffectiveDiffractionIR[i] = SvenssonHelperIntegralSolution( t, v3SourcePos, v3TargetPos, pApex, fSpeedOfSound );
	}

	return true;
}

float SvenssonHelperIntegralSolution( const float t, const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos,
                                      std::shared_ptr<const ITAGeo::CITADiffractionWedgeApertureBase> pApex, const float fSpeedOfSound )
{
	if( fSpeedOfSound <= 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Speed of sound can't be smaller or equal zero for diffraction model" );

	const float fApertureLength        = pApex->GetShortestPathLength( v3SourcePos, v3TargetPos );
	const float fShortestTimeOfArrival = pApex->GetMinimumWavefrontDelayTime( v3SourcePos, v3TargetPos, fSpeedOfSound ); // Also referred to as \Tau_0

	const float theta_W = float( pApex->GetOpeningAngleRad( ) );
	assert( theta_W != 0.0f );

	const float nu = PI_F / theta_W;

	// Integration over dz from z_1 to z_2 (crest, aperture start to aperture end point)
	const int iNumIntegrationSteps = 100; // @todo jst: what integration step over aperture makes sense?
	float fIntegralSolution        = 0.0f;
	VistaVector3D v3IntegrationApexPoint;
	for( size_t i = 0; i < iNumIntegrationSteps; i++ )
	{
		v3IntegrationApexPoint = pApex->v3VertextStart + pApex->GetApertureDirection( ) * fApertureLength * ( float( i ) / float( iNumIntegrationSteps - 1 ) );

		const float m = ( v3IntegrationApexPoint - v3SourcePos ).GetLength( );
		const float l = ( v3IntegrationApexPoint - v3TargetPos ).GetLength( );
		assert( l * m != 0.0f );

		const float fCurrentCrestPointPropagationTime = ( m + l ) / fSpeedOfSound;
		const float fTau                              = fCurrentCrestPointPropagationTime - fShortestTimeOfArrival;

		if( SvenssonHelperHeavisideFunction( t - fTau ) == 1.0f )
		{
			const float fDotProductAlpha = fabs( pApex->GetApertureDirection( ).Dot( ( v3IntegrationApexPoint - v3SourcePos ).GetNormalized( ) ) );
			const float alpha            = acos( fDotProductAlpha );
			assert( alpha != 0.0f );

			const float fDotProductGamma = fabs( pApex->GetApertureDirection( ).Dot( ( v3IntegrationApexPoint - v3TargetPos ).GetNormalized( ) ) );
			const float gamma            = acos( fDotProductGamma );
			assert( gamma != 0.0f );

			// @todo is this depending on current integration apex point or not?
			const float theta_S = pApex->GetIncidenceWaveAngleRad( v3SourcePos );
			const float theta_R = pApex->GetReciprocalIncidenceWaveAngleRad( v3TargetPos );
			fIntegralSolution += SvenssonHelperBeta( nu, theta_S, theta_R, alpha, gamma ) / ( l * m );
		}
	}

	fIntegralSolution *= -nu / 4.0f / PI_F;

	return fIntegralSolution;
}

float SvenssonHelperBeta( const float nu, const float theta_s, const float theta_r, const float alpha, const float gamma )
{
	float fSum = 0.0f;

	const float fVariant1 = PI_F + theta_s - theta_r;
	fSum += sin( SvenssonHelperBetaEval( nu, alpha, gamma, fVariant1 ) );
	const float fVariant2 = PI_F - theta_s - theta_r;
	fSum += sin( SvenssonHelperBetaEval( nu, alpha, gamma, fVariant2 ) );
	const float fVariant3 = PI_F + theta_s + theta_r;
	fSum += sin( SvenssonHelperBetaEval( nu, alpha, gamma, fVariant3 ) );
	const float fVariant4 = PI_F - theta_s + theta_r;
	fSum += sin( SvenssonHelperBetaEval( nu, alpha, gamma, fVariant4 ) );

	return fSum;
}

float SvenssonHelperBetaEval( const float nu, const float alpha, const float gamma, const float fPMVariant )
{
	assert( alpha != 0.0f );
	assert( gamma != 0.0f );
	const float fCosCosDenominator = cos( alpha ) * cos( gamma );
	if( fCosCosDenominator == 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Alpha or gamma can not be zero" );

	const float fDenominator = cosh( nu * acosh( ( 1 + sin( alpha ) * cos( alpha ) ) / fCosCosDenominator ) ) - cos( fPMVariant );
	assert( fDenominator != 0.0f );
	return sin( nu * fPMVariant ) / fDenominator;
}

float SvenssonHelperHeavisideFunction( const float fX )
{
	return ( fX < 0.0f ? 0.0f : 1.0f );
}
