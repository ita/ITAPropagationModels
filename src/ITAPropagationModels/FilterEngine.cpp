﻿#include <ITAPropagationModels/FilterEngine.h>

// ITA includes
#include <ITAException.h>
#include <ITAFFTUtils.h>
#include <ITAFiniteImpulseResponse.h>
#include <ITAGeo/Coordinates.h>
#include <ITAGeo/Directivity/Base.h>
#include <ITAGeo/Directivity/DAFF_Format.h>
#include <ITAGeo/Directivity/DAFF_ImpulseResponse.h>
#include <ITAGeo/Directivity/DAFF_MagnitudeSpectrum.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAISO9613.h>
#include <ITAPropagationModels/Maekawa.h>
#include <ITAPropagationModels/UTD.h>

// Vista includes
#include <VistaMath/VistaGeometries.h>

// STL includes
#include <assert.h>
#include <complex>
#include <stdio.h>


// Spline
#include "../../ITAFFT/include/ITAFFTUtils.h"

#include <spline.h>


using namespace ITAPropagationModels;

CFilterEngine::CFilterEngine( const ITAGeo::ECoordinateSystemConvention iCSC /* = ISO */ ) : m_pMaterialManager( nullptr ), m_iCoordinateSystemConvention( iCSC ) {}

CFilterEngine::~CFilterEngine( ) {}

int CFilterEngine::GetNumSensorChannels( const ITAGeo::CPropagationPathList& oPathList )
{
	// Check for correct structur of oPathList
	if( !ValidateSameSensor( oPathList ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "The propagation path list has multiple sensor anchors or last anchor of paths is not a sensor." );

	// Cast the sensor that is always the last propagation anchor of a path
	auto pSensor = std::dynamic_pointer_cast<ITAGeo::CSensor>( oPathList[0][oPathList[0].size( ) - 1] );
	return pSensor->GetNumChannels( );
}

bool CFilterEngine::ValidateSameSensor( const ITAGeo::CPropagationPathList& oPathList )
{
	std::shared_ptr<ITAGeo::CSensor> pPeviousSensor = nullptr;

	// Last anchor of the all paths must share the same sensor (pointer)
	for( auto& oPath: oPathList )
	{
		if( oPath.empty( ) )
			ITA_EXCEPT_INVALID_PARAMETER( "Detected an empty path in path list during sensor validation, aborting" );

		auto pLastAnchor = oPath[oPath.size( ) - 1];

		if( pLastAnchor->iAnchorType != ITAGeo::CPropagationAnchor::ACOUSTIC_SENSOR )
			return false;

		if( pPeviousSensor && pLastAnchor != pPeviousSensor )
			return false;

		pPeviousSensor = std::dynamic_pointer_cast<ITAGeo::CSensor>( pLastAnchor );
	}

	return true;
}

bool CFilterEngine::ValidateSameEmitter( const ITAGeo::CPropagationPathList& oPathList )
{
	std::shared_ptr<ITAGeo::CEmitter> pPeviousEmitter = nullptr;

	// Last anchor of the all paths must share the same sensor (pointer)
	for( auto& oPath: oPathList )
	{
		if( oPath.empty( ) )
			ITA_EXCEPT_INVALID_PARAMETER( "Detected an empty path in path list during emitter validation, aborting" );

		auto pFirstAnchor = oPath[0];

		if( pFirstAnchor->iAnchorType != ITAGeo::CPropagationAnchor::ACOUSTIC_EMITTER )
			return false;

		if( pPeviousEmitter && pFirstAnchor != pPeviousEmitter )
			return false;

		pPeviousEmitter = std::dynamic_pointer_cast<ITAGeo::CEmitter>( pFirstAnchor );
	}

	return true;
}

/*
void CFilterEngine::ApplyDiffractionModel(ITAGeo::CPropagationPathList & oPathList, int iModel)
{
int iDiffractionModelResolution;
if (iModel == -1)
iDiffractionModelResolution = oM.iDiffractionModel;
else
iDiffractionModelResolution = iModel;

for (auto& oPath : oPathList)
{
//Iterate over all anchors except the first and last anchor
for (size_t i = 1; i <= oPath.size() - 2; i++)
{
auto& pLastAnchor = oPath[i - 1];
auto& pCurrentAnchor = oPath[i];
auto& pNextAnchor = oPath[i + 1];

//If  the anchor is a diffraction and the acoustic material is not set yet, set it to default values
if ((pCurrentAnchor->iAnchorType == ITAGeo::CPropagationAnchor::DIFFRACTION_OUTER_APEX ||
pCurrentAnchor->iAnchorType == ITAGeo::CPropagationAnchor::DIFFRACTION_INNER_APEX) && (pCurrentAnchor->pAcousticMaterial == NULL))
{
auto pApex = std::dynamic_pointer_cast<ITAGeo::CITADiffractionWedgeApertureBase>(pCurrentAnchor);

if (iDiffractionModelResolution == ITAGeo::Material::IMaterial::THIRD_OCTAVE)
{
auto pAcousticMaterial = make_shared<ITAGeo::Material::CThirdOctaveMaterial>();

auto vfFrequencies = pAcousticMaterial->oAbsorptionCoefficients.GetCenterFrequencies();

for (size_t j = 0; j < vfFrequencies.size(); j++)
{
auto fFrequency = vfFrequencies[j];

// TODO: Change diffraction calculation, just for testing
double dDirectLength, dDetourLength;
Maekawa::GetDirectLengthAndDetourLength(pLastAnchor->v3InteractionPoint, pNextAnchor->v3InteractionPoint, pApex, dDirectLength, dDetourLength);
const double dDiffractionFactor = Maekawa::CalculateDiffractionFactor(dDirectLength, dDetourLength, fFrequency, ITAConstants::SPEED_OF_SOUND_F);

// Absorption coefficient alpha = 1 - |D|^2
const float fAlpha = float(1.0f - pow(dDiffractionFactor, 2));
pAcousticMaterial->oAbsorptionCoefficients.SetValue((int)j, fAlpha);
}

pApex->pAcousticMaterial = pAcousticMaterial;
}
else
{
ITA_EXCEPT_NOT_IMPLEMENTED;
}
}
}
}
}
*/

void ITAPropagationModels::CFilterEngine::SetMaterialManager( std::shared_ptr<const ITAGeo::Material::CMaterialManager> pMaterialManager )
{
	m_pMaterialManager = pMaterialManager;
}

std::shared_ptr<const ITAGeo::Material::CMaterialManager> ITAPropagationModels::CFilterEngine::GetMaterialManager( ) const
{
	return m_pMaterialManager;
}

void ITAPropagationModels::CFilterEngine::SetDirectivityManager( std::shared_ptr<const ITAGeo::Directivity::CDirectivityManager> pDirectivityManager )
{
	m_pDirectivityManager = pDirectivityManager;
}

std::shared_ptr<const ITAGeo::Directivity::CDirectivityManager> ITAPropagationModels::CFilterEngine::GetDirectivityManager( ) const
{
	return m_pDirectivityManager;
}

void CFilterEngine::Generate( const ITAGeo::CPropagationPathList& oPathList, ITABase::CHDFTSpectra& oHDFTSpectra, bool* pbDFTDegreeTooSmall )
{
	if( pbDFTDegreeTooSmall )
		*pbDFTDegreeTooSmall = false;

	// Check for correct structur of oPathList
	if( !ValidateSameSensor( oPathList ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "The propagation path list does not share the same sensor." );

	if( !ValidateSameEmitter( oPathList ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "The propagation path list has more than multiple sensor anchors or last anchor of paths is not a sensor." );

	// Initialize the path spectra
	m_pTempPropPathSpectra =
	    std::make_unique<ITABase::CHDFTSpectra>( (float)oHDFTSpectra.GetSampleRate( ), oHDFTSpectra.GetNumChannels( ), oHDFTSpectra.GetDFTSize( ), true );
	m_pAccumulatedSpectra =
	    std::make_unique<ITABase::CHDFTSpectra>( (float)oHDFTSpectra.GetSampleRate( ), oHDFTSpectra.GetNumChannels( ), oHDFTSpectra.GetDFTSize( ), true );


	// Domain-specific temporary variables
	double dScalarMagnitude;                                       // For the combination of all scalar filter components
	ITABase::CThirdOctaveFactorMagnitudeSpectrum oThirdOctFactors; // For the combination of all third octave filter components
	oThirdOctFactors.SetIdentity( );
	ITABase::CMultichannelFiniteImpulseResponse oMultiChannelIR( oHDFTSpectra.GetNumChannels( ), oHDFTSpectra.GetDFTSize( ), (float)oHDFTSpectra.GetSampleRate( ),
	                                                             false );
	// ITABase::CHDFTSpectra oMultichannelIR( oHDFTSpectra.GetDFTSize(), ( float ) oHDFTSpectra.GetSampleRate(), false );

	for( auto& oPath: oPathList )
	{
		// Path length
		const double dPathLength = oPath.GetLength( );
		if( dPathLength == 0.0f )
			ITA_EXCEPT_INVALID_PARAMETER( "Encountered a path with no geometrical distance, could not generate filter" );

		// Include sound energy decrease due to the length of the path @todo this might get more complex with diffraction
		dScalarMagnitude = 1 / dPathLength;

		if( oPropagationModelConfiguration.iAtmosphericAbsorption == EAtmoshpericAbsorption::ISO9613 )
		{
			// Air attenuation according to ISO9613-1 (overrides third octaves, so no resetting required, do not change order!)
			ITABase::ISO9613::AtmosphericAbsorption( oThirdOctFactors, (float)dPathLength, oEnvironmentState.dTemperature, oEnvironmentState.dHumidity );
		}

		// Process path and assemble filter components domain by domain

		for( size_t i = 0; i < oPath.size( ); i++ )
		{
			auto pAnchor( oPath[i] );
			switch( pAnchor->iAnchorType )
			{
				case( ITAGeo::CPropagationAnchor::ACOUSTIC_EMITTER ):
				{
					auto pEmitter = std::dynamic_pointer_cast<const ITAGeo::CEmitter>( pAnchor );

					if( i == oPath.size( ) )
						ITA_EXCEPT_INVALID_PARAMETER( "Propagation path invalid: detected an emitting anchor point at the end of the path." );
					auto pNextAnchor( oPath[i + 1] );

					if( pEmitter->pDirectivity )
					{
						const VistaVector3D v3DirGlobalCS( pNextAnchor->v3InteractionPoint - pEmitter->v3InteractionPoint );
						const VistaVector3D v3DirLocalCS = pEmitter->qOrient.GetInverted( ).Rotate( v3DirGlobalCS );
						ITAGeo::Coordinates::CSpherical oDirection( v3DirLocalCS, m_iCoordinateSystemConvention );
						switch( pEmitter->pDirectivity->GetFormat( ) )
						{
							case( ITAGeo::Directivity::IDirectivity::OPENDAFF ):
							{
								auto pDAFFDirectivity = std::dynamic_pointer_cast<const ITAGeo::Directivity::CDAFF_Format>( pEmitter->pDirectivity );
								if( pDAFFDirectivity->GetDomain( ) == ITADomain::ITA_FREQUENCY_DOMAIN )
								{
									auto pDAFFDirectivityMS = std::dynamic_pointer_cast<const ITAGeo::Directivity::CDAFF_MagnitudeSpectrum>( pEmitter->pDirectivity );
									pDAFFDirectivityMS->MultiplyNearestNeighbourMagnitudeSpectrum( oDirection, oThirdOctFactors );
								}
							}
						}
					}
					break;
				}
				case( ITAGeo::CPropagationAnchor::ACOUSTIC_SENSOR ):
				{
					auto pSensor = std::dynamic_pointer_cast<const ITAGeo::CSensor>( pAnchor );

					if( i == 0 )
						ITA_EXCEPT_INVALID_PARAMETER( "Propagation path invalid: detected a sensor anchor point at the beginning of the path (without a predecessor)." );
					auto pPreviousAnchor( oPath[i - 1] );

					if( pSensor->pDirectivity )
					{
						const VistaVector3D v3DirGlobalCS( pPreviousAnchor->v3InteractionPoint - pSensor->v3InteractionPoint );
						const VistaVector3D v3DirLocalCS = pSensor->qOrient.GetInverted( ).Rotate( v3DirGlobalCS );
						ITAGeo::Coordinates::CSpherical oDirection( v3DirLocalCS, m_iCoordinateSystemConvention );
						switch( pSensor->pDirectivity->GetFormat( ) )
						{
							case( ITAGeo::Directivity::IDirectivity::OPENDAFF ):
							{
								auto pDAFFDirectivity = std::dynamic_pointer_cast<const ITAGeo::Directivity::CDAFF_Format>( pSensor->pDirectivity );
								if( pDAFFDirectivity->GetDomain( ) == ITADomain::ITA_FREQUENCY_DOMAIN )
								{
									auto pDAFFDirectivityMS = std::dynamic_pointer_cast<const ITAGeo::Directivity::CDAFF_MagnitudeSpectrum>( pSensor->pDirectivity );
									pDAFFDirectivityMS->MultiplyNearestNeighbourMagnitudeSpectrum( oDirection, oThirdOctFactors );
								}
								else if( pDAFFDirectivity->GetDomain( ) == ITADomain::ITA_TIME_DOMAIN )
								{
									auto pDAFFDirectivityIR = std::dynamic_pointer_cast<const ITAGeo::Directivity::CDAFF_ImpulseResponse>( pSensor->pDirectivity );
									pDAFFDirectivityIR->GetNearestNeighbourImpulseResponse( oDirection, oMultiChannelIR );
									oMultiChannelIR.CyclicShift( int( pDAFFDirectivityIR->GetMeanTimeOfArrival( ) ) );
								}
							}
								// case(ITAGeo::Directivity::IDirectivity::???):
						}
					}
					break;
				}
				case( ITAGeo::CPropagationAnchor::SPECULAR_REFLECTION ):
				{
					auto pReflection = std::dynamic_pointer_cast<const ITAGeo::CSpecularReflection>( pAnchor );
					if( pReflection->pMaterial )
					{
						// @todo jst: switch() case: include octave resolution
						if( pReflection->pMaterial->GetType( ) == ITAGeo::Material::IMaterial::THIRD_OCTAVE )
						{
							// Type cast of the acoustic material
							auto pThirdOctaveMaterial = std::dynamic_pointer_cast<ITAGeo::Material::CThirdOctaveMaterial>( pReflection->pMaterial );

							// Multiply the scale values of the respective bands
							for( int j = 0; j < pThirdOctaveMaterial->GetNumBands( ); j++ )
								oThirdOctFactors[j] *= pThirdOctaveMaterial->GetTransmissionFactor( j );
						}
					}
					break;
				}
			}
		}

		// Combine domains


		ITAFFTUtils::Convert( oMultiChannelIR, m_pTempPropPathSpectra.get( ) );

		for( int iChannel = 0; iChannel < m_pTempPropPathSpectra->GetNumChannels( ); iChannel++ )
		{
			auto pTempPathSpectrum = ( *m_pTempPropPathSpectra )[iChannel];
			if( iChannel == 0 )
			{
				// Interpolate third octaves and multiply calculated values with third octave and scalar factors and add delay due to sound propagation
				for( int i = 0; i < pTempPathSpectrum->GetSize( ); i++ )
				{
					float* pData = (float*)&( oThirdOctFactors.GetCenterFrequencies( )[0] );
					const float fThirdOctaveInterp =
					    spline_b_val( oThirdOctFactors.GetNumBands( ), pData, &( oThirdOctFactors[0] ), m_pTempPropPathSpectra->GetFrequencyOfBin( i ) );

					std::complex<float> cfMultipliedSpectra = pTempPathSpectrum->GetCoeff( i ) * fThirdOctaveInterp * (float)dScalarMagnitude;

					pTempPathSpectrum->SetCoeff( i, cfMultipliedSpectra );

					// Get the delay [samples] and calculate the phase shift according to the shift theorem
					// DFT(x(k-Delta)) = exp(-j * omega_k * delta) * X(k) ,    with omega_k = 2 * pi * k / N
					float fDelaySamples = float( dPathLength / oEnvironmentState.fSpeedOfSound * oHDFTSpectra.GetSampleRate( ) );
					float fDelayPhase   = -fDelaySamples * ITAConstants::TWO_PI_F * i / (float)oHDFTSpectra.GetDFTSize( );

					pTempPathSpectrum->SetPhasePreserveMagnitude( i, fDelayPhase );
				}
			}
			else
			{
				// Copy first channel to other channels
				pTempPathSpectrum->Copy( ( *m_pTempPropPathSpectra )[0] );
			}
		}

		// Add current spectra to accumulated spectra
		m_pAccumulatedSpectra->add( m_pTempPropPathSpectra.get( ) );
	}

	oHDFTSpectra.CopyFrom( m_pAccumulatedSpectra.get( ) );
}
