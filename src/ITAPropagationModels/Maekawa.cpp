#include <ITAException.h>
#include <ITAPropagationModels/Maekawa.h>
#include <cassert>

using namespace ITAGeo;
using namespace ITAPropagationModels;

bool Maekawa::IsApplicable( const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos, std::shared_ptr<const ITAGeo::CITADiffractionWedgeApertureBase> pApex )
{
	return pApex->IsOccluding( v3SourcePos, v3TargetPos );
}

void Maekawa::GetDirectLengthAndDetourLength( const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos,
                                              std::shared_ptr<const ITAGeo::CITADiffractionWedgeApertureBase> pApex, double& dPropagationLengthFreeField,
                                              double& dPropagationLengthOverApex )
{
	dPropagationLengthOverApex  = ( pApex->v3InteractionPoint - v3SourcePos ).GetLength( ) + ( v3TargetPos - pApex->v3InteractionPoint ).GetLength( );
	dPropagationLengthFreeField = ( v3TargetPos - v3SourcePos ).GetLength( );

	assert( dPropagationLengthOverApex > 0.0f );
	assert( dPropagationLengthFreeField > 0.0f );
}

double Maekawa::KirchhoffDelta( const double dPropagationLengthFreeField, const double dPropagationLengthOverApex )
{
	if( dPropagationLengthFreeField <= 0.0f )
		ITA_EXCEPT1( INVALID_PARAMETER, "Diffraction direct path length can not be zero or negative" );

	if( dPropagationLengthOverApex <= 0.0f )
		ITA_EXCEPT1( INVALID_PARAMETER, "Diffraction detour path length can not be zero or negative" )

	if( dPropagationLengthOverApex < dPropagationLengthFreeField )
		ITA_EXCEPT1( INVALID_PARAMETER, "Diffraction detour path length can not be smaller than direct path" );

	const double dKirchhoffDelta = dPropagationLengthOverApex - dPropagationLengthFreeField; // Difference in path length, no magic here
	return dKirchhoffDelta;
}

void Maekawa::CalculateDiffractionFilter( const double dDirect, const double dDetour, ITABase::CHDFTSpectrum& oTransferFunction,
                                          const float fSpeedOfSound /*= ITAConstants::SPEED_OF_SOUND_F*/ )
{
	assert( fSpeedOfSound > 0.0f );
	if( fSpeedOfSound <= 0.0f )
		ITA_EXCEPT1( INVALID_PARAMETER, "Speed of sound can't be zero or negative" );

	// Per definition, DC value (N = 0) is set to or 10^(5/20) / r_dir (5dB loss as well as loss due to detour length)
	float fCoefficient = pow( 10.0f, 5.0f / 20.0f ) / dDirect;
	oTransferFunction.SetCoeffRI( 0, fCoefficient, 0.0f );


	const int iDFTSize = oTransferFunction.GetSize( );
	for( int i = 1; i < iDFTSize; i++ )
	{
		const float fFrequency = oTransferFunction.GetFrequencyResolution( ) * i;
		fCoefficient           = (float)CalculateDiffractionFilterCoefficient( dDirect, dDetour, fFrequency, fSpeedOfSound );
		oTransferFunction.SetCoeffRI( i, fCoefficient );

		// Set wave number and phase shift
		const float k = ITAConstants::TWO_PI_F_L * fFrequency / fSpeedOfSound;
		oTransferFunction.SetPhasePreserveMagnitude( i, -k * (float)dDetour );
	}
}

double Maekawa::CalculateDiffractionFilterCoefficient( const double dPropagationLengthFreeField, const double dPropagationLengthOverApex, const float fFrequency,
                                                       const float fSpeedOfSound /*= ITAConstants::SPEED_OF_SOUND_F */ )
{
	// Difference between detour length and direct length
	const double dDifference = dPropagationLengthOverApex - dPropagationLengthFreeField;

	// Fresnel number N according to Maekawa //TODO: Equation number
	const double dN = 2.0 * dDifference / fSpeedOfSound * fFrequency;

	// Calculation of the diffraction filter coefficient according to Kurze, Anderson
	const double dTemp              = sqrt( ITAConstants::TWO_PI_D * dN );
	const double dFilterCoefficient = pow( pow( 10.0, 5.0 / 20.0 ) * dTemp / tanh( dTemp ), -1 ) / dPropagationLengthFreeField;


	return dFilterCoefficient;
}


double Maekawa::CalculateDiffractionFactor( const double dPropagationLengthFreeField, const double dPropagationLengthOverApex, const float fFrequency,
                                            const float fSpeedOfSound /*= ITAConstants::SPEED_OF_SOUND_F */ )
{
	// Return the diffraction factor without the influence of the propagation path length
	return dPropagationLengthOverApex * CalculateDiffractionFilterCoefficient( dPropagationLengthFreeField, dPropagationLengthOverApex, fFrequency, fSpeedOfSound );
}