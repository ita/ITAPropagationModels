#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Directivity/Utils.h>
#include <ITAGeo/Material/Utils.h>
#include <ITAPropagationModels/Utils.h>
#include <cassert>

float ITAPropagationModels::Utils::EstimateFilterLengthSamples( const ITAGeo::CPropagationPath& oPath, const float fSampleRate, const float fMinimumSamples,
                                                                const float fSpeedOfSound )
{
	float fEstimatedFilterLengthSamples = fSampleRate * (float)oPath.GetLength( );
	for( auto a: oPath )
	{
		switch( a->iAnchorType )
		{
			case ITAGeo::CPropagationAnchor::ACOUSTIC_EMITTER:
			{
				auto p = std::dynamic_pointer_cast<const ITAGeo::CEmitter>( a );
				if( p->pDirectivity )
					fEstimatedFilterLengthSamples += ITAGeo::Directivity::Utils::EstimateFilterLengthSamples( p->pDirectivity, fSampleRate, fSpeedOfSound );
				break;
			}
			case ITAGeo::CPropagationAnchor::ACOUSTIC_SENSOR:
			{
				auto p = std::dynamic_pointer_cast<const ITAGeo::CSensor>( a );
				if( p->pDirectivity )
					fEstimatedFilterLengthSamples += ITAGeo::Directivity::Utils::EstimateFilterLengthSamples( p->pDirectivity, fSampleRate, fSpeedOfSound );
				break;
			}
			case ITAGeo::CPropagationAnchor::SPECULAR_REFLECTION:
			{
				auto p = std::dynamic_pointer_cast<const ITAGeo::CSpecularReflection>( a );
				if( p->pMaterial )
					fEstimatedFilterLengthSamples += ITAGeo::Material::Utils::EstimateFilterLengthSamples( p->pMaterial, fSampleRate, fSpeedOfSound );
				break;
			}
			default:
			{
				if( fEstimatedFilterLengthSamples < fMinimumSamples )
					fEstimatedFilterLengthSamples = fMinimumSamples;
			}
		}
	}

	return fEstimatedFilterLengthSamples;
}
