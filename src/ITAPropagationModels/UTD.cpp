#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAGeo/Utils.h>
#include <ITAPropagationModels/UTD.h>
#include <cassert>

using namespace ITAConstants;
using namespace ITAPropagationModels;
//! Solves the Fresnel integral for given value, in literature often referred to as F( X )
std::complex<float> UTDHelperFunctionFresnelIntegralSolution( const float fX );

//! Returns the approximated Fresnel integral solution introduced by Kawai for two cases below and above argument value at 0.8
std::complex<float> UTDHelperFunctionKawaiApproximationFresnelIntegral( const float fX );

//! Helper function for case-prone angular distance measure, in literature often referred to as "a+"
/**
 * @param[in] fBeta Angular measure for distance to critical zones, i.e. shadow and reflection boundary
 * @param[in] n Wedge opening angle devided by Pi ( angle = n * pi, see literature )
 */
float UTDHelperFunction_a_plus( const float fBeta, const float n );

//! Helper function for case-prone angular distance measure, in literature often referred to as "a-"
/**
 * @param[in] fBeta Angular measure for distance to critical zones, i.e. shadow and reflection boundary
 * @param[in] n Wedge opening angle devided by Pi ( angle = n * pi, see literature )
 */
float UTDHelperFunction_a_minus( const float fBeta, const float n );


void UTD::CalculateDiffractionFilter( const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos,
                                      std::shared_ptr<const ITAGeo::CITADiffractionWedgeApertureBase> pApex, ITABase::CHDFTSpectrum& oTF,
                                      const int iMethod /* = UTD_APPROX_KAWAI_KOUYOUMJIAN */, const float fSpeedOfSound /* = ITAConstants::SPEED_OF_SOUND_F */ )
{
	std::complex<float> cfCoeff;
	oTF.SetCoeffRI( 0, 1.0f, 0.0f ); // DC
	for( int i = 1; i < oTF.GetSize( ); i++ )
	{
		const float fFrequency = float( i ) * oTF.GetFrequencyResolution( );
		assert( fFrequency > 0.0f );
		UTD::CalculateDiffractionCoefficient( v3SourcePos, v3TargetPos, pApex, fFrequency, cfCoeff, iMethod );
		oTF.SetCoeff( i, cfCoeff );
	}
}

bool UTD::CalculateDiffractionCoefficient( const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos,
                                           std::shared_ptr<const ITAGeo::CITADiffractionWedgeApertureBase> pApex, const float fFrequency, std::complex<float>& cfCoeff,
                                           const int iMethod /* = UTD_APPROX_KAWAI_KOUYOUMJIAN */, const float fSpeedOfSound /* = ITAConstants::SPEED_OF_SOUND_F */ )
{
	if( ITAGeoUtils::IsPointInsideDiffrationWedge( pApex->v3VertextStart, pApex->v3VertextEnd, pApex->v3MainWedgeFaceNormal, pApex->v3OppositeWedgeFaceNormal,
	                                               v3SourcePos ) )
		return false;

	if( ITAGeoUtils::IsPointInsideDiffrationWedge( pApex->v3VertextStart, pApex->v3VertextEnd, pApex->v3MainWedgeFaceNormal, pApex->v3OppositeWedgeFaceNormal,
	                                               v3TargetPos ) )
		return false;


	// UTD algorithm

	const float n = float( pApex->GetOpeningAngleRad( ) / PI_D );
	assert( n != 0.0f );

	const float theta_i = pApex->GetIncidenceWaveAngleRad( v3SourcePos );
	assert( theta_i != 0.0f );

	const float k = TWO_PI_F_L * fFrequency / fSpeedOfSound;
	;
	assert( k > 0.0f );

	const std::complex<float> j( 0.0f, 1.0f ); // Complex value "j" (engineering) or "i" (math, physics)

	const std::complex<float> cfNominator   = -1.0f * exp( -j * PI_F / 4.0f );
	const std::complex<float> cfDenominator = 2.0f * n * sqrt( 2.0f * k * PI_F ) * sin( theta_i );

	// TODO: GetWedgeMainFaceElevationToPointRad() doesn't return the correct angle
	const float alpha_i = ITAGeoUtils::GetWedgeMainFaceElevationToPointRad( pApex->v3VertextStart, pApex->v3VertextEnd, pApex->v3MainWedgeFaceNormal, v3SourcePos );
	const float alpha_d = ITAGeoUtils::GetWedgeMainFaceElevationToPointRad( pApex->v3VertextStart, pApex->v3VertextEnd, pApex->v3MainWedgeFaceNormal, v3TargetPos );

	const float fAlphaSum  = alpha_d + alpha_i;
	const float fAlphaDiff = alpha_d - alpha_i;

	const float rho = ( pApex->v3AperturePoint - v3SourcePos ).GetLength( );
	const float r   = ( v3TargetPos - pApex->v3AperturePoint ).GetLength( );

	const float L = rho * r / ( rho + r ) * sin( theta_i ) * sin( theta_i ); // Spherical wave distance parameter

	const float fCotArg1 = ( PI_F + fAlphaDiff ) / ( 2.0f * n );
	const float fCotArg2 = ( PI_F - fAlphaDiff ) / ( 2.0f * n );
	const float fCotArg3 = ( PI_F + fAlphaSum ) / ( 2.0f * n );
	const float fCotArg4 = ( PI_F - fAlphaSum ) / ( 2.0f * n );

	std::complex<float> cfTerm1, cfTerm2, cfTerm3, cfTerm4;
	if( iMethod == UTD_APPROX_KAWAI_KOUYOUMJIAN )
	{
		// Account for singularities in shadow and reflection boundary regions

		const float fAngleThresholdSingularityRad = 0.0f; // @todo = 2.0f * PI_F / 10;
		if( fmodf( std::abs( fAlphaDiff ), PI_F ) < fAngleThresholdSingularityRad || fmodf( std::abs( fAlphaSum ), PI_F ) < fAngleThresholdSingularityRad )
		{
			// Kouyoumjian approx
			ITA_EXCEPT_NOT_IMPLEMENTED;
			const float fEpsilon       = 0.0f; // @todo
			const float fEpsilonSignum = fEpsilon > 0 ? 1.0f : -1.0f;
			cfTerm1 = n * exp( -j * PI_F / 4.0f ) * ( sqrt( 2.0f * PI_F * k * L ) * fEpsilonSignum - 2.0f * k * L * fEpsilon * exp( -j * PI_F / 4.0f ) );
			cfTerm2 = cfTerm1;
			cfTerm3 = cfTerm1;
			cfTerm4 = cfTerm1;
		}
		else
		{
			// Kawai approx, equation 22

			const float fX1 = k * L * UTDHelperFunction_a_plus( fAlphaDiff, n );
			cfTerm1         = tan( PI_F / 2.0f - fCotArg1 ) * UTDHelperFunctionKawaiApproximationFresnelIntegral( fX1 );
			const float fX2 = k * L * UTDHelperFunction_a_minus( fAlphaDiff, n );
			cfTerm2         = tan( PI_F / 2.0f - fCotArg2 ) * UTDHelperFunctionKawaiApproximationFresnelIntegral( fX2 );
			const float fX3 = k * L * UTDHelperFunction_a_plus( fAlphaSum, n );
			cfTerm3         = tan( PI_F / 2.0f - fCotArg3 ) * UTDHelperFunctionKawaiApproximationFresnelIntegral( fX3 );
			const float fX4 = k * L * UTDHelperFunction_a_minus( fAlphaSum, n );
			cfTerm4         = tan( PI_F / 2.0f - fCotArg4 ) * UTDHelperFunctionKawaiApproximationFresnelIntegral( fX4 );
		}
	}
	else if( iMethod == UTD_FRESNEL_INTEGRAL_FORM )
	{
		// @todo jst, but run in not implemented exception ...

		const float fX1 = k * L * UTDHelperFunction_a_plus( fAlphaDiff, n );
		cfTerm1         = 1.0f / tan( fCotArg1 ) * UTDHelperFunctionFresnelIntegralSolution( fX1 );
		const float fX2 = k * L * UTDHelperFunction_a_minus( fAlphaDiff, n );
		cfTerm2         = 1.0f / tan( fCotArg2 ) * UTDHelperFunctionFresnelIntegralSolution( fX2 );
		const float fX3 = k * L * UTDHelperFunction_a_plus( fAlphaSum, n );
		cfTerm3         = 1.0f / tan( fCotArg3 ) * UTDHelperFunctionFresnelIntegralSolution( fX3 );
		const float fX4 = k * L * UTDHelperFunction_a_minus( fAlphaSum, n );
		cfTerm4         = 1.0f / tan( fCotArg4 ) * UTDHelperFunctionFresnelIntegralSolution( fX4 );
	}
	else
	{
		ITA_EXCEPT1( INVALID_PARAMETER, "Unrecognized UTD diffraction coefficient algorithm" );
	}

	// Diffraction coefficient formula
	std::complex<float> cfD = ( cfNominator / cfDenominator ) * ( cfTerm1 + cfTerm2 + cfTerm3 + cfTerm4 ); // D( .. )

	// Amplitude scaling factor
	float fA = sqrt( rho / ( r * ( rho + r ) ) ) * sin( theta_i );

	// Complete diffraction coefficient with A and influence of rho
	cfCoeff = cfD * fA / rho * exp( -j * k * ( rho + r ) );

	return true;
}

bool CalculateDiffractionFactor( const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos,
                                 std::shared_ptr<const ITAGeo::CITADiffractionWedgeApertureBase> pApex, const float fFrequency, std::complex<float>& cfFactor,
                                 const int iMethod /* = UTD_APPROX_KAWAI_KOUYOUMJIAN*/, const float fSpeedOfSound /* = ITAConstants::SPEED_OF_SOUND_F */ )
{
	ITA_EXCEPT_NOT_IMPLEMENTED;
}

std::complex<float> UTDHelperFunctionFresnelIntegralSolution( const float fX )
{
	const std::complex<float> j( 0.0f, 1.0f );                // Complex helper value, "j" or "i"
	const std::complex<float> cfIntegralResult( 0.0f, 0.0f ); // @todo jst: solve.
	const std::complex<float> cfResult = ( 2.0f * j * sqrt( fX ) * exp( j * fX ) ) * cfIntegralResult;

	ITA_EXCEPT1( NOT_IMPLEMENTED, "Integral UTD solution is not implemented, use approximation instead." );

	// return cfResult;
}

std::complex<float> UTDHelperFunctionKawaiApproximationFresnelIntegral( const float fX )
{
	const std::complex<float> j( 0.0f, 1.0f ); // Complex helper value, "j" or "i"

	// Some sanity
	assert( fX >= 0.0f );
	if( fX < 0.0f )
		return 0.0f;

	// Phase term is always the same
	const std::complex<float> cfTermPhase = exp( -j * PI_F / 4.0f * ( 1 - sqrt( fX / ( fX + 1.4f ) ) ) );

	if( fX < 0.8f )
	{
		const float fTermAmplitude1 = sqrt( PI_F * fX );
		const float fTermAmplitude2 = 1 - sqrt( fX ) / ( 0.7f * sqrt( fX ) + 1.2f );
		return ( fTermAmplitude1 * fTermAmplitude2 * cfTermPhase );
	}
	else
	{
		const float fTermAmplitude = 1 - 0.8f / ( ( fX + 1.25f ) * ( fX + 1.25f ) );
		return ( fTermAmplitude * cfTermPhase );
	}
}

float UTDHelperFunction_a_plus( const float fBeta, const float n )
{
	const float fN_plus = fBeta <= PI_F * ( n - 1 ) ? 0.0f : 1.0f;
	const float fCosArg = ( 2.0f * PI_F * n * fN_plus - fBeta ) / 2.0f;
	return 2 * cos( fCosArg ) * cos( fCosArg );
}

float UTDHelperFunction_a_minus( const float fBeta, const float n )
{
	float fN_minus = 0.0f;
	if( fBeta < PI_F * ( n - 1 ) )
		fN_minus = -1.0f;
	else if( fBeta > PI_F * ( n + 1 ) )
		fN_minus = 1.0f;

	const float fCosArg = ( 2.0f * PI_F * n * fN_minus - fBeta ) / 2.0f;
	return 2 * cos( fCosArg ) * cos( fCosArg );
}
