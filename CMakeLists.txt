cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

init_project ()

project (
	ITAPropagationModels
	VERSION 2024.0
	LANGUAGES CXX C
)

# Options
option (ITA_PROPAGATION_MODELS_WITH_TESTS "Build the tests for the library" OFF)
option (ITA_PROPAGATION_MODELS_WITH_BENCHMARKS "Build the benchmarks for the library" OFF)

# Library
ihta_add_library (
	NAME ${PROJECT_NAME}
	SOURCES include/ITAPropagationModels/Base.h
			include/ITAPropagationModels/Definitions.h
			include/ITAPropagationModels/FilterEngine.h
			include/ITAPropagationModels/Maekawa.h
			include/ITAPropagationModels/Svensson.h
			include/ITAPropagationModels/UTD.h
			include/ITAPropagationModels/Utils.h
			include/ITAPropagationModels/Atmosphere/AirAttenuation.h
			src/ITAPropagationModels/FilterEngine.cpp
			src/ITAPropagationModels/Maekawa.cpp
			src/ITAPropagationModels/Svensson.cpp
			src/ITAPropagationModels/UTD.cpp
			src/ITAPropagationModels/Utils.cpp
			src/ITAPropagationModels/Atmosphere/AirAttenuation.cpp
	NAMESPACE ${PROJECT_NAME}
	IDE_FOLDER "ITAGeometricalAcoustics"
	OUT_LIB LIB_TARGET
)

# Linking
target_link_libraries (
	${LIB_TARGET}
	PUBLIC ITABase::ITABase ITAGeo::ITAGeo ITAPropagationPathSim::ITAPropagationPathSim
	PRIVATE ITAFFT::ITAFFT Spline::spline
)

# Definitions for Shared/Static
target_compile_definitions (
	${LIB_TARGET}
	PUBLIC $<IF:$<BOOL:${BUILD_SHARED_LIBS}>,ITA_PROPAGATION_MODELS_EXPORT,ITA_PROPAGATION_MODELS_STATIC>
)

# Organize sources in folders
GroupSourcesByFolder (${LIB_TARGET})

# Install & export
packageProject (
	NAME ${PROJECT_NAME}
	VERSION ${PROJECT_VERSION}
	BINARY_DIR ${PROJECT_BINARY_DIR}
	INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include
	INCLUDE_DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
	DEPENDENCIES "ITABase;ITAGeo;ITAFFT;ITAPropagationPathSim;spline"
	COMPATIBILITY ExactVersion
	NAMESPACE ${PROJECT_NAME}
	DISABLE_VERSION_SUFFIX YES
)

# Benchmarks
if (ITA_PROPAGATION_MODELS_WITH_BENCHMARKS)
	add_subdirectory (benchmarks)
endif ()

# Tests
if (ITA_PROPAGATION_MODELS_WITH_TESTS)
	add_subdirectory (tests)
endif ()
