cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (ITAPropagationModelsTest)

# ######################################################################################################################

add_executable (SvenssonTest SvenssonTest.cpp)
target_link_libraries (SvenssonTest PUBLIC ITAPropagationModels::ITAPropagationModels ITAFFT::ITAFFT)

set_property (TARGET SvenssonTest PROPERTY FOLDER "Tests/ITAPropagationModels")
set_property (TARGET SvenssonTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS SvenssonTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################
