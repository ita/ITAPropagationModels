/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAFFTUtils.h>
#include <ITAFiniteImpulseResponse.h>
#include <ITAPropagationModels/Svensson.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <VistaBase/VistaVector3D.h>
#include <cassert>
#include <cmath>
#include <iostream>
#include <vistaBase/VistaExceptionBase.h>

using namespace std;
using namespace ITAConstants;
using namespace ITAGeo;
using namespace ITAPropagationModels;

static const float g_fSampleRate   = 44.1e3f;
static const float g_fSpeedOfSound = ITAConstants::DEFAULT_SPEED_OF_SOUND_F;

//! Tests running the Svensson model functions
/**
 *
 */
int main( int, char** )
{
	auto pS = std::make_shared<CEmitter>( VistaVector3D( -2.0f, 0.0f, 0.0f ) );
	auto pR = std::make_shared<CSensor>( VistaVector3D( 2.0f, 0.0f, 0.0f ) );

	auto pW = std::make_shared<CITADiffractionOuterWedgeAperture>( );
	pW->v3AperturePoint.SetValues( 0.0f, 1.0f, 0.0f );
	pW->v3MainWedgeFaceNormal.SetValues( -1.0f, 1.0f, .0f );
	pW->v3MainWedgeFaceNormal.Normalize( );
	pW->v3OppositeWedgeFaceNormal.SetValues( 1.0f, 1.0f, .0f );
	pW->v3OppositeWedgeFaceNormal.Normalize( );
	pW->v3VertextStart.SetValues( .0f, 1.0f, -1.0f );
	pW->v3VertextEnd.SetValues( .0f, 1.0f, 1.0f );

	CPropagationPath oPropPath;
	oPropPath.push_back( pS );
	oPropPath.push_back( pW );
	oPropPath.push_back( pR );

	cout << oPropPath << endl;

	try
	{
		const float fMinDelay = pW->GetMinimumWavefrontDelayTime( pS->v3InteractionPoint, pR->v3InteractionPoint, g_fSpeedOfSound );
		const float fMaxDelay = pW->GetMaximumWavefrontDelayTime( pS->v3InteractionPoint, pR->v3InteractionPoint, g_fSpeedOfSound );

		// Calculate effective IR (only part of propagation path that is affected by diffraction)
		const int iIRLength = (int)ceil( float( fMaxDelay - fMinDelay ) * g_fSampleRate );
		ITABase::CFiniteImpulseResponse oEffectiveDiffractionIR( iIRLength, g_fSampleRate, true );

		ITAStopWatch sw;
		sw.start( );
		Svensson::CalculateDiffractionIR( pS->v3InteractionPoint, pR->v3InteractionPoint, pW, oEffectiveDiffractionIR );
		cout << "Svensson IR generation calculation time: " << timeToString( sw.stop( ) ) << endl;

		cout << oEffectiveDiffractionIR.toString( ) << endl;

		// Determine complete IR including delay
		const int iIROnsetSample       = (int)floor( float( fMinDelay ) * g_fSampleRate );
		const int iIRFullLengthSamples = (int)ceil( float( fMaxDelay ) * g_fSampleRate );
		ITABase::CFiniteImpulseResponse oTransferPathIR( iIRFullLengthSamples + 123, g_fSampleRate, true ); // give some extra head room after diffraction impulse
		oTransferPathIR.write( oEffectiveDiffractionIR.GetData( ), oEffectiveDiffractionIR.GetLength( ), iIROnsetSample );
		oTransferPathIR.Store( "SvenssonTestIR.wav" );
	}
	catch( const VistaExceptionBase& e )
	{
		cerr << e.GetPrintStatement( ) << endl;
		return 255;
	}
	catch( const ITAException& e )
	{
		cerr << e << endl;
		return 255;
	}

	return 0;
}
