/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAFFTUtils.h>
#include <ITAPropagationModels/UTD.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <VistaBase/VistaVector3D.h>
#include <cassert>
#include <iostream>
#include <vistaBase/VistaExceptionBase.h>

using namespace std;
using namespace ITAConstants;
using namespace ITAGeo;
using namespace ITAPropagationModels;

static const float g_fSampleRate   = 44.1e3f;
static const int g_iFilterLength   = 128;
static const float g_fSpeedOfSound = ITAConstants::DEFAULT_SPEED_OF_SOUND_F;

//! Tests running the UTD model functions
/**
 *
 */
int main( int, char** )
{
	auto pS = std::make_shared<CEmitter>( VistaVector3D( -2.0f, 0.0f, 0.0f ) );
	auto pR = std::make_shared<CSensor>( VistaVector3D( 2.0f, 0.0f, 0.0f ) );

	auto pW = std::make_shared<CITADiffractionOuterWedgeAperture>( );
	pW->v3AperturePoint.SetValues( 0.0f, 1.0f, 0.0f );
	pW->v3MainWedgeFaceNormal.SetValues( -1.0f, 1.0f, .0f );
	pW->v3MainWedgeFaceNormal.Normalize( );
	pW->v3OppositeWedgeFaceNormal.SetValues( 1.0f, 1.0f, .0f );
	pW->v3OppositeWedgeFaceNormal.Normalize( );
	pW->v3VertextStart.SetValues( .0f, 1.0f, 1.0f );
	pW->v3VertextEnd.SetValues( .0f, 1.0f, -1.0f );

	CPropagationPath oPropPath;
	oPropPath.push_back( pS );
	oPropPath.push_back( pW );
	oPropPath.push_back( pR );

	cout << oPropPath << endl;

	try
	{
		ITABase::CHDFTSpectrum oDiffractionTF( g_fSampleRate, g_iFilterLength / 2 + 1, true );
		ITAStopWatch sw;
		sw.start( );

		std::complex<float> cfCoeff;
		oDiffractionTF.SetCoeffRI( 0, 1.0f, 0.0f );
		for( int i = 1; i < oDiffractionTF.GetSize( ); i++ )
		{
			const float f = float( i ) * float( oDiffractionTF.GetFrequencyResolution( ) );
			const float k = 2.0f * PI_F * f / g_fSpeedOfSound;
			UTD::CalculateDiffractionCoefficient( pS->v3InteractionPoint, pR->v3InteractionPoint, pW, k, cfCoeff, UTD::UTD_APPROX_KAWAI_KOUYOUMJIAN );
			oDiffractionTF.SetCoeff( i, cfCoeff );
		}

		cout << "UTD filter generation calculation time: " << timeToString( sw.stop( ) ) << endl;

		cout << oDiffractionTF.ToString( ) << endl;

		ITAFFTUtils::Export( &oDiffractionTF, "UTDTestIR.wav" );
	}
	catch( const VistaExceptionBase& e )
	{
		cerr << e.GetPrintStatement( ) << endl;
		return 255;
	}
	catch( const ITAException& e )
	{
		cerr << e << endl;
		return 255;
	}

	return 0;
}
