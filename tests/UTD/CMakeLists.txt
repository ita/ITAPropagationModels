cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (ITAPropagationModelsTest)

# ######################################################################################################################

add_executable (UTDTest UTDTest.cpp)
target_link_libraries (UTDTest PUBLIC ITAPropagationModels::ITAPropagationModels ITAFFT::ITAFFT)

set_property (TARGET UTDTest PROPERTY FOLDER "Tests/ITAPropagationModels")
set_property (TARGET UTDTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS UTDTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################
