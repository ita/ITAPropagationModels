/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */


#include <ITAFFTUtils.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Directivity/Base.h>
#include <ITAGeo/Directivity/DAFF_ImpulseResponse.h>
#include <ITAGeo/Directivity/DAFF_MagnitudeSpectrum.h>
#include <ITAPropagationModels/FilterEngine.h>
#include <ITAPropagationModels/Utils.h>

using namespace std;
using namespace ITAConstants;
using namespace ITAGeo;
using namespace ITAPropagationModels;

const float fSampleRate = 44.1e3f;

int main( int, char** )
{
	auto pSenderLeft  = make_shared<CEmitter>( VistaVector3D( -2.0f, 0.0f, 0.0f ) );
	auto pSenderRight = make_shared<CEmitter>( VistaVector3D( 2.0f, 0.0f, 0.0f ) );

	auto pHRIR = make_shared<Directivity::CDAFF_ImpulseResponse>( );
	pHRIR->LoadFromFile( "ITA_Artificial_Head_5x5_44kHz_128.v17.ir.daff" );

	auto pReceiver          = make_shared<CSensor>( VistaVector3D( 0.0f, 0.0f, 0.0f ) );
	pReceiver->pDirectivity = pHRIR;

	CPropagationPath oPathFromLeft;
	oPathFromLeft.push_back( pSenderLeft );
	oPathFromLeft.push_back( pReceiver );

	CPropagationPath oPathFromRight;
	oPathFromRight.push_back( pSenderRight );
	oPathFromRight.push_back( pReceiver );

	CFilterEngine oFilterEngine( OPENGL );
	oFilterEngine.oPropagationModelConfiguration.iAtmosphericAbsorption = CFilterEngine::EAtmoshpericAbsorption::NONE;
	oFilterEngine.oPropagationModelConfiguration.iGeometricalSpreading  = CFilterEngine::EGeometricalSpreading::PLANE_WAVE;

	int iFilterLengthSamples = (int)ceil( Utils::EstimateFilterLengthSamples( oPathFromLeft, fSampleRate ) );
	ITABase::CHDFTSpectra oTransmissionFilterLeft( fSampleRate, pReceiver->GetNumChannels( ), iFilterLengthSamples );
	bool bDFTDegreeTooSmallFlag;
	oFilterEngine.Generate( oPathFromLeft, oTransmissionFilterLeft, &bDFTDegreeTooSmallFlag );

	if( bDFTDegreeTooSmallFlag )
		cerr << "DFT lengh too small, could not include all path completely into target filter" << endl;

	ITAFFTUtils::Export( &oTransmissionFilterLeft, "SourceFromLeft.wav" ); // Exports in time domain as impulse response (IR)


	ITABase::CHDFTSpectra oTransmissionFilterRight( fSampleRate, pReceiver->GetNumChannels( ), iFilterLengthSamples );
	oFilterEngine.Generate( oPathFromRight, oTransmissionFilterRight, &bDFTDegreeTooSmallFlag );

	if( bDFTDegreeTooSmallFlag )
		cerr << "DFT lengh too small, could not include all path completely into target filter" << endl;

	ITAFFTUtils::Export( &oTransmissionFilterRight, "SourceFromRight.wav" );
}
