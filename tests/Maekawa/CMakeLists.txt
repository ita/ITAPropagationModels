cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (ITAPropagationModelsTest)

# ######################################################################################################################

add_executable (MaekawaTest MaekawaTest.cpp)
target_link_libraries (MaekawaTest PUBLIC ITAPropagationModels::ITAPropagationModels ITAFFT::ITAFFT)

set_property (TARGET MaekawaTest PROPERTY FOLDER "Tests/ITAPropagationModels")
set_property (TARGET MaekawaTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS MaekawaTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################
