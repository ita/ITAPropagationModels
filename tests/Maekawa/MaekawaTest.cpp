/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAFFTUtils.h>
#include <ITAPropagationModels/Maekawa.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <VistaBase/VistaVector3D.h>
#include <cassert>
#include <iostream>
#include <vistaBase/VistaExceptionBase.h>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationModels;

static float g_fSampleRate = 44.1e3f;
static int g_iFilterLength = 1280;

//! Tests running the Maekawa model functions
/**
 *
 */
int main( int, char** )
{
	auto pS = std::make_shared<CEmitter>( VistaVector3D( -2.0f, 0, 0 ) );
	auto pR = std::make_shared<CSensor>( VistaVector3D( 2.0f, 0, 0 ) );

	auto pW = std::make_shared<CITADiffractionOuterWedgeAperture>( );
	pW->v3AperturePoint.SetValues( 0.0f, 1.0f, 0.0f );
	pW->v3MainWedgeFaceNormal.SetValues( -1.0f, 1.0f, .0f );
	pW->v3MainWedgeFaceNormal.Normalize( );
	pW->v3OppositeWedgeFaceNormal.SetValues( 1.0f, 1.0f, .0f );
	pW->v3OppositeWedgeFaceNormal.Normalize( );
	pW->v3VertextStart.SetValues( .0f, 1.0f, -1.0f );
	pW->v3VertextEnd.SetValues( .0f, 1.0f, 1.0f );

	CPropagationPath oPropPath;
	oPropPath.push_back( pS );
	oPropPath.push_back( pW );
	oPropPath.push_back( pR );

	cout << oPropPath << endl;

	try
	{
		if( Maekawa::IsApplicable( pS->v3InteractionPoint, pR->v3InteractionPoint, pW ) )
			cout << "[OK] Maekawa method can be applied for this path" << endl;
		else
			cout << "[BAD] Maekawa method can't be applied for this path, trying anyway" << endl;

		double dPropLengthDirect, dPropLengthDetour;
		Maekawa::GetDirectLengthAndDetourLength( pS->v3InteractionPoint, pR->v3InteractionPoint, pW, dPropLengthDirect, dPropLengthDetour );

		cout << "Maekawa direct path length: " << dPropLengthDirect << " m" << endl;
		cout << "Maekawa detour path length: " << dPropLengthDetour << " m" << endl;
		cout << "Kirchhoff delta: " << Maekawa::KirchhoffDelta( dPropLengthDirect, dPropLengthDetour ) << " m" << endl;

		ITABase::CHDFTSpectrum oDiffractionTF( g_fSampleRate, g_iFilterLength / 2 + 1, true );
		ITAStopWatch sw;
		sw.start( );
		Maekawa::CalculateDiffractionFilter( dPropLengthDirect, dPropLengthDetour, oDiffractionTF );
		cout << "Maekawa filter generation calculation time: " << timeToString( sw.stop( ) ) << endl;

		if( Maekawa::IsApplicable( pS->v3InteractionPoint, pR->v3InteractionPoint, pW ) )
		{
			oDiffractionTF.SetZero( );
		}

		cout << oDiffractionTF.ToString( ) << endl;

		ITAFFTUtils::Export( &oDiffractionTF, "MaekawaTestIR.wav" );
	}
	catch( const VistaExceptionBase& e )
	{
		cerr << e.GetPrintStatement( ) << endl;
		return 255;
	}
	catch( const ITAException& e )
	{
		cerr << e << endl;
		return 255;
	}

	return 0;
}
