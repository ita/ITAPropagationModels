%% Load C++ implementation result
maekawa_ir_cpp = ita_read( 'MaekawaTestIR.wav' );
maekawa_ir_cpp.signalType = 'energy';

%% Run ITA-Toolbox diffraction simulation
n1 = [ -1 1 0 ];
n2 = [ 1 1 0 ];
loc = [ 0 1 0 ];
w = itaInfiniteWedge( n1, n2, loc );

s = [ -2 0 0 ];
r = [ 2 0 0 ];

c = 344.0;

maekawa_ir = maekawa_ir_cpp;
maekawa_ir.freqData = ita_diffraction_maekawa( w, s, r, maekawa_ir_cpp.freqVector, c );

utd_ir = maekawa_ir_cpp;
utd_ir.freqData = ita_diffraction_utd( w, s, r, maekawa_ir_cpp.freqVector, c );
%% Combined result

maekawa_eval = ita_merge( maekawa_ir, maekawa_ir_cpp,utd_ir );

%% Plot result

maekawa_eval.pt