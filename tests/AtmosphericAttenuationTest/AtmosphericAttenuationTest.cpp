/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAPropagationModels/Atmosphere/AirAttenuation.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>

//#include <ITAException.h>
#include <ITAStopWatch.h>
#include <iostream>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationModels;
using namespace ITAPropagationPathSim;


void TestAttenuation( )
{
	auto oAtmosphere = CStratifiedAtmosphere( );


	const VistaVector3D v3SourcePos = VistaVector3D( 0, 0, 2000 );
	auto oRay                       = AtmosphericRayTracing::CRay( v3SourcePos, 135, 0 );
	const VistaVector3D v3WfNormal  = oRay.InitialDirection( );

	const int ciNPoints = 102;
	for( int idx = 1; idx <= ciNPoints; idx++ )
	{
		const double dDeltaR = idx * 2000.0 / ciNPoints;
		const double dTime   = idx * 6 / ciNPoints;
		oRay.Append( v3SourcePos + v3WfNormal * dDeltaR, v3WfNormal, dTime );
	}


	ITABase::CThirdOctaveFactorMagnitudeSpectrum oAttenuationSpectrum;
	Atmosphere::AirAttenuationSpectrum( oAttenuationSpectrum, oRay, oAtmosphere );
	cout << "Spectrum using all ray points:" << endl << oAttenuationSpectrum << endl << endl;

	Atmosphere::AirAttenuationSpectrum( oAttenuationSpectrum, oRay, oAtmosphere, 5 );
	cout << "Spectrum using every fifth ray point:" << endl << oAttenuationSpectrum << endl << endl;
}

void BenchmarkAttenuation( )
{
	auto oAtmosphere = CStratifiedAtmosphere( );

	const VistaVector3D v3SourcePos = VistaVector3D( 0, 0, 2000 );
	auto oRay                       = AtmosphericRayTracing::CRay( v3SourcePos, 135, 0 );
	const VistaVector3D v3WfNormal  = oRay.InitialDirection( );

	const int ciNPoints = 1003;
	for( int idx = 1; idx <= ciNPoints; idx++ )
	{
		const double dDeltaR = idx * 3000.0 / ciNPoints;
		const double dTime   = idx * 10 / ciNPoints;
		oRay.Append( v3SourcePos + v3WfNormal * dDeltaR, v3WfNormal, dTime );
	}


	ITAStopWatch watch;
	ITABase::CThirdOctaveFactorMagnitudeSpectrum oSpectrum;
	for( int idx = 0; idx < 100; idx++ )
	{
		watch.start( );
		Atmosphere::AirAttenuationSpectrum( oSpectrum, oRay, oAtmosphere );
		watch.stop( );
	}
	cout << "Benchmark for " << ciNPoints << " segments:" << endl << watch << endl << endl;

	watch.reset( );
	const int iDownSamplingFactor = 5;
	for( int idx = 0; idx < 100; idx++ )
	{
		watch.start( );
		Atmosphere::AirAttenuationSpectrum( oSpectrum, oRay, oAtmosphere, iDownSamplingFactor );
		watch.stop( );
	}
	cout << "Benchmark for " << std::ceil( (double)ciNPoints / (double)iDownSamplingFactor ) << " segments (downsampling of factor " << iDownSamplingFactor
	     << "):" << endl
	     << watch << endl
	     << endl;


	watch.reset( );
	for( int idx = 0; idx < 100; idx++ )
	{
		watch.start( );
		oSpectrum = Atmosphere::AirAttenuationSpectrum( oRay, oAtmosphere );
		watch.stop( );
	}
	cout << "Benchmark for " << ciNPoints << " segments returning a spectrum:" << endl << watch << endl << endl;
}


int main( int, char** )
{
	TestAttenuation( );

	BenchmarkAttenuation( );

	return 0;
}
