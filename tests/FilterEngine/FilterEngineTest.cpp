/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */


#include <ITAFFTUtils.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAISO9613.h>
#include <ITAPropagationModels/FilterEngine.h>

using namespace std;
using namespace ITAConstants;
using namespace ITAGeo;
using namespace ITAPropagationModels;

int main( int, char** )
{
	auto pSource   = make_shared<CEmitter>( VistaVector3D( -2.0f, 0.0f, 0.0f ) );
	auto pReceiver = make_shared<CSensor>( VistaVector3D( 2.0f, 0.0f, 0.0f ) );

	auto pMaterialDirectory = std::make_shared<Material::CMaterialManager>( "./" );
	auto pReflection        = make_shared<CSpecularReflection>( VistaVector3D( 0.0f, 2.0f, 0.0f ) );
	pReflection->pMaterial  = pMaterialDirectory->GetMaterial( "stonewall" );

	auto pW = std::make_shared<CITADiffractionOuterWedgeAperture>( );
	pW->v3AperturePoint.SetValues( 0.0f, 1.0f, 0.0f );
	pW->v3MainWedgeFaceNormal.SetValues( -1.0f, 1.0f, .0f );
	pW->v3MainWedgeFaceNormal.Normalize( );
	pW->v3OppositeWedgeFaceNormal.SetValues( 1.0f, 1.0f, .0f );
	pW->v3OppositeWedgeFaceNormal.Normalize( );
	pW->v3VertextStart.SetValues( .0f, 1.0f, -1.0f );
	pW->v3VertextEnd.SetValues( .0f, 1.0f, 1.0f );

	CPropagationPath oPathDirect;
	oPathDirect.push_back( pSource );
	oPathDirect.push_back( pReceiver );

	CPropagationPath oPathReflection;
	oPathReflection.push_back( pSource );
	oPathReflection.push_back( pReflection );
	oPathReflection.push_back( pReceiver );

	CPropagationPath oPathDiffraction;
	oPathDiffraction.push_back( pSource );
	oPathDiffraction.push_back( pW );
	oPathDiffraction.push_back( pReceiver );

	CPropagationPathList oPathList;
	oPathList.push_back( oPathDirect );
	oPathList.push_back( oPathReflection );
	oPathList.push_back( oPathDiffraction );

	CFilterEngine oFilterEngine;
	oFilterEngine.SetMaterialManager( pMaterialDirectory );

	// Set filter length according to the maximum path length
	const float fSpeedOfSound = DEFAULT_SPEED_OF_SOUND_F; // Approximation of speed of sound at ~20�C
	const float fSampleRate   = 44.1e3f;
	int iFilterLengthSamples  = (int)( oPathList.GetMaxLength( ) / fSpeedOfSound * fSampleRate );
	iFilterLengthSamples      = int( iFilterLengthSamples + 4096 );

	ITABase::CHDFTSpectra oTransmissionFilter( fSampleRate, pReceiver->GetNumChannels( ), iFilterLengthSamples );
	bool bDFTDegreeTooSmallFlag;
	oFilterEngine.Generate( oPathList, oTransmissionFilter, &bDFTDegreeTooSmallFlag );

	if( bDFTDegreeTooSmallFlag )
		cerr << "DFT lengh too small, could not include all path completely into target filter" << endl;

	ITAFFTUtils::Export( &oTransmissionFilter, "FilterEngineTest.wav" ); // Exports in time domain as impulse response (IR)
}
