/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */


#include <ITAFFTUtils.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Directivity/Base.h>
#include <ITAGeo/Directivity/DAFF_ImpulseResponse.h>
#include <ITAGeo/Directivity/DAFF_MagnitudeSpectrum.h>
#include <ITAPropagationModels/FilterEngine.h>
#include <ITAPropagationModels/Utils.h>

using namespace std;
using namespace ITABase;
using namespace ITAConstants;
using namespace ITAGeo;
using namespace ITAFFTUtils;
using namespace ITAPropagationModels;

const float fSampleRate = 44.1e3f;

int main( int, char** )
{
	shared_ptr<Directivity::CDAFF_MagnitudeSpectrum> pTrumpetDirectivity = make_shared<Directivity::CDAFF_MagnitudeSpectrum>( );
	pTrumpetDirectivity->LoadFromFile( "Trumpet1.v17.ms.daff" );

	auto pSender          = make_shared<CEmitter>( VistaVector3D( 2.0f, 1.3f, 2.0f ) );
	pSender->pDirectivity = pTrumpetDirectivity;

	auto pReceiver = make_shared<CSensor>( VistaVector3D( 0.0f, 0.0f, 0.0f ) );

	CPropagationPath oPath;
	oPath.push_back( pSender );
	oPath.push_back( pReceiver );

	CFilterEngine oFilterEngine( OPENGL );

	int iFilterLengthSamples = (int)ceil( Utils::EstimateFilterLengthSamples( oPath, fSampleRate ) );
	CHDFTSpectra oTransmissionFilter( fSampleRate, pReceiver->GetNumChannels( ), iFilterLengthSamples );
	bool bDFTDegreeTooSmallFlag;
	oFilterEngine.Generate( oPath, oTransmissionFilter, &bDFTDegreeTooSmallFlag );

	if( bDFTDegreeTooSmallFlag )
		cerr << "DFT lengh too small, could not include all path completely into target filter" << endl;

	Export( &oTransmissionFilter, "SourceDirectivityTest.wav" ); // Exports in time domain as impulse response (IR)
}
